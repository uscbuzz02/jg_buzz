/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javagochi;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author admin
 */
public class GochiSplashView {
    private JFrame frame;
    private JPanel mainPane;
    private JPanel sidePane;
    private JButton crtPetBtn;
    private JButton viewPetBtn;
    private JButton discPetBtn;
    private JButton extBtn;
    private JPanel avatarPane;
    private JPanel statPane;
    private JLabel nameLbl;
    private JLabel lvlLbl;
    private JLabel HPLbl;
    private JLabel SPLbl;
    private JLabel EPLbl;
    private JLabel petTypeLbl;
    private String petName;
    
            
    private GochiController ctrl;
    
    public GochiSplashView(GochiController gc){
        this.ctrl = gc;
        initComponents();
    }

    private void initComponents(){
        //instantiate & initialize FRAME
        this.frame = new JFrame("JavaGochi");
        this.frame.setSize(400, 300);
        this.frame.setVisible(true);
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        this.frame.setLayout(new BorderLayout());
        
        this.sidePane = new JPanel(new GridLayout(4,1));
        this.sidePane.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        this.sidePane.setSize(100,280);
        
        this.mainPane = new JPanel(new GridLayout(1,2));
        this.mainPane.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        this.mainPane.setSize(200,100);
        
        this.frame.add(this.sidePane);
        this.frame.add(this.mainPane);
        
        this.crtPetBtn = new JButton("Create Pet");
        this.viewPetBtn = new JButton("View Pet");
        this.discPetBtn = new JButton("Discard Pet");
        this.extBtn = new JButton("Exit");
        
        //ADD buttons to SIDEPANE
       this.sidePane.add(this.crtPetBtn);
       this.sidePane.add(this.viewPetBtn);
       this.sidePane.add(this.discPetBtn);
       this.sidePane.add(this.extBtn);
        
        this.avatarPane = new JPanel();
        this.avatarPane.setSize(150,300);
        this.avatarPane.setBorder(BorderFactory.createLineBorder(Color.BLACK));
       
        
        //INITIALIZE STATPANE
        this.statPane = new JPanel(new GridLayout(5,1));
        this.statPane.setSize(50,300);
        this.statPane.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
        //CREATE LABELS
        this.petTypeLbl = new JLabel();
        this.nameLbl = new JLabel("Peta");
        this.lvlLbl = new JLabel("1");
        this.HPLbl = new JLabel("100/100");
        this.SPLbl = new JLabel("100/100");
        this.EPLbl = new JLabel("100/100");
        
        //ADD STATPANE & AVATARPANE to MAINPANE
        this.mainPane.add(this.avatarPane);
        this.mainPane.add(this.statPane);
        
        //ADD LABELS to STATPANE
        this.statPane.add(this.nameLbl);
        this.statPane.add(this.lvlLbl);
        this.statPane.add(this.HPLbl);
        this.statPane.add(this.SPLbl);
        this.statPane.add(this.EPLbl);
        
        //add event managers
        this.crtPetBtn.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
               petName = JOptionPane.showInputDialog(null, "New Pet", JOptionPane.QUESTION_MESSAGE);
               
               System.out.println("New pet created: "+petName);
               ctrl.submitPetName(petName);
            }
        });
        
        
    }
    
    /**
     * @return the frame
     */
    public JFrame getFrame() {
        return frame;
    }

    /**
     * @return the mainPane
     */
    public JPanel getMainPane() {
        return mainPane;
    }

    /**
     * @return the sidePane
     */
    public JPanel getSidePane() {
        return sidePane;
    }

    /**
     * @return the crtPetBtn
     */
    public JButton getCrtPetBtn() {
        return crtPetBtn;
    }

    /**
     * @return the viewPetBtn
     */
    public JButton getViewPetBtn() {
        return viewPetBtn;
    }

    /**
     * @return the discPetBtn
     */
    public JButton getDiscPetBtn() {
        return discPetBtn;
    }

    /**
     * @return the extBtn
     */
    public JButton getExtBtn() {
        return extBtn;
    }
    
    
}
